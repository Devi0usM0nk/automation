#!/bin/bash

ssWeb(){
	currentDate=$(date  +%m:%d:%y_%T)
	local tmp_random_id=$(($(shuf -i 1-$RANDOM -n1)*$RANDOM))
	local INF="[SubdomainScan:$tmp_random_id][INF]"
	local FTL="[SubdomainScan:$tmp_random_id][FTL]"
	buffer=$(echo "$tmp/buffer$tmp_random_id")
	destructor(){
		if  ps -p $aquatone_pid &>/dev/null ;then kill -9 $aquatone_pid&>/dev/null;fi
		[[ -f  "$tmp/screenshots$tmp_random_id/aquatone_report.html" ]] && mv "$tmp/screenshots$tmp_random_id/aquatone_report.html" "$current/ssWeb/aquatone_report$currentDate.html" &>/dev/null
		[[ -d  "$tmp/screenshots$tmp_random_id/screenshots" ]] && mv $tmp/screenshots$tmp_random_id/screenshots/* "$current/ssWeb/screenshots/" &> /dev/null
		[[ -d  "$tmp/screenshots$tmp_random_id/html" ]] && mv $tmp/screenshots$tmp_random_id/html/* "$current/ssWeb/html/" &> /dev/null
		[[ -d  "$tmp/screenshots$tmp_random_id/headers" ]] && mv $tmp/screenshots$tmp_random_id/headers/* "$current/ssWeb/headers/" &> /dev/null
		[[ -f  "$tmp/screenshots$tmp_random_id/aquatone_sessions.json" ]] && cat "$tmp/screenshots$tmp_random_id/aquatone_session.json"  2> /dev/null>> "$current/ssWeb/aquatone_session.json" 
		[[ -f  "$tmp/screenshots$tmp_random_id/aquatone_urls.txt" ]] && cat "$tmp/screenshots$tmp_random_id/aquatone_urls.txt"  2> /dev/null>> "$current/ssWeb/aquatone_urls.txt" 
		rm $tmp/* 2>/dev/null
	}

	
	echo "$INF Started ssWeb"
	[[ ! -f "$current/httpxOut"  ]] && echo "$FTL $current/httpxOut does not exists" && exit 1
	[[ ! -d "$current/ssWeb" ]] && mkdir "$current/ssWeb" "$current/ssWeb/screenshots" "$current/ssWeb/html" "$current/ssWeb/headers" && echo "$INF Created Directory $current/screenshot"
	trap "destructor && exit 1" SIGINT SIGTERM
	mkdir "$tmp/screenshots$tmp_random_id" 
	cat "$current/httpxOut" | tail -n 50 | awk '{print $1}' | aquatone -http-timeout 30000 -save-body=false -out "$tmp/screenshots$tmp_random_id/" &>/dev/null &
	local aquatone_pid=$!
	echo "$INF Started Aquatone..."
	wait

	destructor
}
