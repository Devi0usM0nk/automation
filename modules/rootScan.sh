#!/bin/bash

rootScan (){
	local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
	local FTL="[RootScan:$tmp_random_id][FTL]"
	local INF="[RootScan:$tmp_random_id][INF]"
	local subfinder_pid=""
	local amass_pid=""
	local assetfinder_pid=""
	declare -a id_array=()
	local root_array
	root_array=$(cat "$current/rootDomains") 
	buffer=$(echo "$tmp/buffer$tmp_random_id") 

	destructor(){
		if ps -p $subfinder_pid &> /dev/null;then kill -9 $subfinder_pid &>/dev/null; fi
		if ps -p $amass_pid &> /dev/null; then  kill -9 $amass_pid &>/dev/null; fi
		if ps -p $assetfinder_pid &> /dev/null; then kill -9 $assetfinder_pid &>/dev/null; fi
		for i in ${gs_pid[@]}
		do
		    if ps -p $i &> /dev/null; then kill -9 $i &>/dev/null;fi
		done

		cat "$current/rootDomains" "$current/nonRootDomains" > "$buffer"
		[[ -f "$tmp/output_amass$tmp_random_id" ]] && cat "$tmp/output_amass$tmp_random_id" 2> /dev/null >> "$buffer"
		[[ -f "$tmp/output_subfinder$tmp_random_id" ]] && cat "$tmp/output_subfinder$tmp_random_id" 2> /dev/null  >> "$buffer"
		[[ -f "$tmp/output_assetfinder$tmp_random_id" ]] && cat "$tmp/output_assetfinder$tmp_random_id" 2> /dev/null  >> "$buffer" 

		for i in ${root_array[@]}
		do
		    [[ -f "$tmp/$i/gs$tmp_random_id" ]] && cat "$tmp/$i/gs$tmp_random_id" 2> /dev/null  >> "$buffer"  
		done

		not_in "$current/all" >> "$tmp/all$tmp_random_id"
		rm "$tmp/output_amass$tmp_random_id" "$tmp/output_subfinder$tmp_random_id" "$tmp/output_assetfinder$tmp_random_id" &>/dev/null

		[[ -f "$tmp/all$tmp_random_id" ]] && cat "$current/all" "$tmp/all$tmp_random_id" 2>/dev/null | sort -u -o "$current/all" && rm "$tmp/all$tmp_random_id"  &>/dev/null && echo "$INF Stored output to $current/all"
		[[ -f "$buffer" ]] && rm "$buffer" &>/dev/null 
		echo "$INF Cleared temporary files"

	}

	echo "$INF Started RootScan"
	[[ ! -f "$current/rootDomains" ]] && echo "$FTL $current/rootDomains Not Found" && exit 1
	# If file all does not exist create
	[[ ! -f "$current/all" ]] && touch "$current/all" && echo "$INF Created $current/all"
	cp "$current/all" "$previous/all"

	trap "destructor && exit 1" SIGINT SIGTERM
	
	subfinder -silent -rL Resolvers -t 250 -dL "$current/rootDomains" -o "$tmp/output_subfinder$tmp_random_id" -all -recursive & 
	local subfinder_pid=$! 
	echo "$INF Started Subfinder..."

	amass enum -passive -silent -df "$current/rootDomains" -rf Resolvers -nolocaldb -o "$tmp/output_amass$tmp_random_id" & 
	local amass_pid=$!
	echo "$INF Started Amass..."

	cat "$current/rootDomains" | assetfinder -w &> "$tmp/output_assetfinder$tmp_random_id" &
	local assetfinder_pid=$!
	echo "$INF Started Assetfinder..."

	for i in $root_array
	do
		[[ ! -d "$tmp/$i" ]] &&	mkdir $tmp/$i
		[[ ! -f "$tmp/$i/gs$tmp_random_id" ]] && touch $tmp/$i/gs$tmp_random_id
		github-subdomains -d $i -t $github_apikey -o $tmp/$i/gs$tmp_random_id &
		gs_pid+=($!)
		echo "$INF Started Github-Subdomain for $i ..."
	done
	
	wait

	cat "$tmp/output_amass$tmp_random_id" "$tmp/output_subfinder$tmp_random_id" "$tmp/output_assetfinder$tmp_random_id" 2> /dev/null | subfinder -r Resolvers -silent  >> "$buffer"
	not_in "$current/all" >> "$tmp/all$tmp_random_id"
	rm "$tmp/output_amass$tmp_random_id" "$tmp/output_subfinder$tmp_random_id" "$tmp/output_assetfinder$tmp_random_id" &>/dev/null

	cat "$current/rootDomains" | xargs -n1 -I{} sh -c " cat \"$tmp/all$tmp_random_id\" | awk -F'{}' '{print $1}' " | sort -u | sed 's/\./\n/g' | sed 's/\s//g'  >> "$current/subdomain_wordlists"
	./clean_wordlists.sh "$(echo $current/subdomain_wordlists| sed 's/\s/\ /g' )" &> /dev/null && cat "$current/subdomain_wordlists_cleaned" "$current/subdomain_wordlists" | sort -u > "$current/subdomain_wordlists" && rm "$current/subdomain_wordlists_cleaned"
	echo "$INF Stored output to Subdomain Wordlist at $current/subdomain_wordlists"

	destructor
}
