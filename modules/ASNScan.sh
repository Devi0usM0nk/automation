#!/bin/bash

ASNScan (){
local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
source functions.sh
local INF="[ASNScan:$tmp_random_id][INF]"
local FTL="[ASNScan:$tmp_random_id][FTL]"
destructor(){
	if ps -p $metabigor_pid &> /dev/null; then kill -9 $metabigor_pid &>/dev/null; fi 
	[[ -f $buffer ]] && not_in $current/CIDR >> $tmp/CIDR$tmp_random_id && rm $buffer
	[[ -f $tmp/CIDR$tmp_random_id ]] && cat $current/CIDR $tmp/CIDR$tmp_random_id | sed 's/\s//g' | sort -u -o $current/CIDR && rm $tmp/CIDR$tmp_random_id && echo "$INF Stored ouput CIDRs in $current/CIDR"
	echo "$INF Cleared temporary files"
}
echo "$INF Started ASN Scan"
[[ ! -f $current/ASN ]] && echo "$FTL $current/ASN does not exists" && exit 1
[[ ! -f $current/CIDR ]] && touch $current/CIDR && echo "$INF Created $current/CIDR"
cp $current/CIDR $previous/CIDR
trap "destructor && exit 1" SIGINT SIGTERM 

cat $current/ASN | metabigor net --asn --accurate -o $buffer &
local metabigor_pid=$!
echo "$INF Started Metabigor..."
wait

not_in $current/CIDR >> $tmp/CIDR$tmp_random_id

destructor
}
