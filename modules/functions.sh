#!/bin/bash
current="inp_op/current"
previous="inp_op/previous"
buffer="tmp/buffer$tmp_random_id"

local FTL="[ScopeParser:$tmp_random_id][FTL]"
local INF="[ScopeParser:$tmp_random_id][INF]"

source $HOME/.local/bin/modules/scopeParse.sh
source $HOME/.local/bin/modules/rootScan.sh
source $HOME/.local/bin/modules/allScan.sh
source $HOME/.local/bin/modules/subdomainScan.sh
source $HOME/.local/bin/modules/IPScan.sh
source $HOME/.local/bin/modules/ssWeb.sh

not_in (){
	cat "$1" "$buffer" | sort -u
}

isSource (){
	[[ ! -f "$1" ]] && echo "$FTL $1 does not exists \!\!\!" && exit 1
}

isDestination (){
	[[ ! -f "$1" ]] && touch "$1" && echo "$INF Created $current/rootDomains"

}
