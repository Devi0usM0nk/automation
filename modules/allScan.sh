#!/bin/bash

allScan (){
	local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
	local FTL="[AllScan:$tmp_random_id][FTL]"
	local INF="[AllScan:$tmp_random_id][INF]"
	local shuffledns_pid=""
	buffer=$(echo "$tmp/buffer$tmp_random_id")


	
	destructor (){
		if ps -p $shuffledns_pid &> /dev/null; then kill -9 $shuffledns_pid &>/dev/null; fi
		[[ -f "$current/out_of_scope" ]] && [[ -f "$tmp/subdomains$tmp_random_id" ]] && cat $current/out_of_scope | xargs -n1 -I {} sh -c "cat $tmp/subdomains$tmp_random_id | grep -v \"{}\" | sort -u -o $tmp/subdomains$tmp_random_id"
		[[ -f "$tmp/subdomains$tmp_random_id" ]] && cat $tmp/subdomains$tmp_random_id $current/subdomains | sort -u -o $current/subdomains && rm $tmp/subdomains$tmp_random_id && echo "$INF Stored output subdomains in $current/subdomains"
		[[ -f "$tmp/IPs$tmp_random_id" ]] && cat $tmp/IPs$tmp_random_id $current/IPs | sort -u -o $current/IPs && rm $tmp/IPs$tmp_random_id && echo "$INF Stored output IPs in $current/IPs"
		rm $buffer
		echo "$INF Cleared temporary files"

	}
	echo "$INF Started AllScan"
	[[ ! -f $current/all ]] && echo "$FTL $current/all Does not exists \!\!\!" && exit 1
	[[ ! -f $current/subdomains ]] && touch $current/subdomains && echo "$INF Created $current/subdomains"
	[[ ! -f $current/IPs ]] && touch $current/IPs && echo "$INF Created $current/IPs"
	trap "destructor && exit 1" SIGINT SIGTERM

	cp $current/subdomains $previous/subdomains
	cp $current/IPs $previous/IPs
	
	
	shuffledns -list "$current/all" -r Resolvers -strict-wildcard -retries 20 -nC | dnsx -a -resp -o $buffer &
	local shuffledns_pid=$!
	echo "$INF Started ShuffleDNS..."
	wait

	not_in $current/subdomains >> $tmp/subdomains$tmp_random_id
	
	cat $tmp/subdomains$tmp_random_id | awk '{print $2}' | sed 's/[][]//g' > $buffer
	not_in $current/IPs >> $tmp/IPs$tmp_random_id

	destructor	
	
}
