#!/bin/bash

scopeParser(){
	source ./functions.sh
	current="./current"
	#local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
	#local FTL="[ScopeParser:$tmp_random_id][FTL]"
	#local INF="[ScopeParser:$tmp_random_id][INF]"
	#buffer=$(echo "$tmp/buffer$tmp_random_id") 

	#destructor(){
	#	[[ -f "$tmp/rootDomains$tmp_random_id" ]] && cat "$current/rootDomains" "$tmp/rootDomains$tmp_random_id" | sort -u -o "$current/rootDomains" && rm "$tmp/rootDomains$tmp_random_id"  && echo "$INF Stored output to $current/rootDomains"
	#	[[ -f "$tmp/nonRootDomains$tmp_random_id" ]] && cat "$current/nonRootDomains" "$tmp/nonRootDomains$tmp_random_id" | sort -u -o "$current/nonRootDomains" && rm "$tmp/nonRootDomains$tmp_random_id" && echo "$INF Stored output to $current/nonRootDomains"
	#	[[ -f "$buffer" ]] && rm "$buffer"
	#	cat "$current/rootDomains" "$current/nonRootDomains" | xargs -n1 -I{} sh -c 'githunter.sh {}'
	#	echo "$INF Cleared temporary files"
	#}
		
	echo "$INF Started Scope Parser"
	isSource "$current/scope"
	
	isDestination "$current/rootDomains"
	isDestination "$current/nonRootDomains"

	#[[ ! -f "$current/scope" ]] && echo "$FTL $current/scope does not exists \!\!\!" && exit 1
	#[[ ! -f "$current/rootDomains" ]] && touch "$current/rootDomains" && echo "$INF Created $current/rootDomains"
	#[[ ! -f "$current/nonRootDomains" ]] && touch "$current/nonRootDomains" && echo "$INF Created $current/nonRootDomains"
	
	#trap "destructor && exit 1" SIGINT SIGTERM

	cat "$current/scope" | grep -v '*' | anew "$current/nonRootDomains"
	# not_in "$current/nonRootDomains" >> "$tmp/nonRootDomains$tmp_random_id"
	
	cat "$current/scope" | grep '*.' | sed 's/\*\.//g' | sort -u | anew "$current/rootDomains"
	#not_in "$current/rootDomains" | sed 's/\s//g' >> "$tmp/rootDomains$tmp_random_id"
	
	#destructor	
}
scopeParser
