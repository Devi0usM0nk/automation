#!/bin/bash

RDNSLookup (){
local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
local FTL="[rDNSLookup:$tmp_random_id][FTL]"
local INF="[rDNSLookup:$tmp_random_id][INF]"
local dnsx_pid=""
buffer=$(echo "$tmp/buffer$tmp_random_id")


destructor (){
	if ps -p $dnsx_pid &>/dev/null ; then kill -9 $dnsx_pid &>/dev/null;fi

	[[ -f $tmp/PTR$tmp_random_id ]] && cat $tmp/PTR$tmp_random_id $current/PTR | sort -u -o $current/PTR && rm $tmp/PTR$tmp_random_id && echo "$INf Stored output PTR to $current/PTR"
	[[ -f $tmp/subdomains$tmp_random_id ]] && cat $tmp/subdomains$tmp_random_id $current/subdomains | sort -u -o $current/subdomains && rm $tmp/subdomains$tmp_random_id && echo "$INf Stored subdomains extracted from PTR to $current/subdomains"
	rm $buffer
	echo "$INF Cleared temporary files"
}

echo "$INF Started RDNS Lookup"
[[ ! -f $current/IPs ]] && echo "$FTL $current/IPs does not exists \!\!\!" && exit 1
[[ ! -f $current/rootDomains ]] && echo "$FTL $current/rootDomains does not exists \!\!\!" && exit 1
[[ ! -f $current/PTR ]] && touch $current/PTR && echo "$INF Created $current/PTR"
[[ ! -f $current/subdomains ]] && touch $current/subdomains && echo "$INF Created $current/subdomains"
cp $current/PTR $previous/PTR
cp $current/subdomains $previous/subdomains
trap "destructor && exit 1" SIGINT SIGTERM

cat $current/IPs | dnsx -resp-only -ptr -silent | dnsx -a -resp -silent -o $buffer  &
local dnsx_pid=$!
echo "$INF Started dnsx..."
wait

not_in $current/PTR >> $tmp/PTR$tmp_random_id

cat $current/rootDomains | xargs -n1 -I{} sh -c "grep .{} $tmp/PTR$tmp_random_id | awk '{print $1}' " > $buffer
not_in $current/subdomains >> $tmp/subdomains$tmp_random_id 

cat $tmp/subdomains$tmp_random_id $current/subdomains | sort -u -o $current/subdomains && echo "$INf Stored subdomains extracted from PTR to $current/subdomains"
cat $tmp/PTR$tmp_random_id $current/PTR | sort -u -o $current/PTR && echo "$INf Stored output PTR to $current/PTR"


rm $tmp/subdomains$tmp_random_id $tmp/PTR$tmp_random_id $buffer	
}

IPScan (){
local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
local FTL="[IPScan:$tmp_random_id][FTL]"
local INF="[IPScan:$tmp_random_id][INF]"
local naabu_pid=""

destructor (){
	if ps -p $naabu_pid &>/dev/null; then sudo kill -9 naabu_pid &>/dev/null; fi

	[[ -f $buffer.json ]] && sudo chown $USER $buffer.json && mv $buffer.json $buffer && not_in $current/PortScan >> $tmp/PortScan$tmp_random_id && rm $buffer
	[[ -f $tmp/PortScan$tmp_random_id ]] && cat $tmp/PortScan$tmp_random_id $current/PortScan | sort -u -o $current/PortScan && rm $tmp/PortScan$tmp_random_id && echo "$INF Stored Port Scan output to $current/PortScan" 
	echo "$INF Cleared temporary file"
	if ps -p $naabu_pid &>/dev/null; then sudo kill -9 naabu_pid &>/dev/null; fi
}

[[ ! -f $current/IPs ]] && echo "$FTL $current/IPs does not exists \!\!\!" && exit 1
[[ ! -f $current/PortScan ]] && touch $current/PortScan && echo "$INF Created $current/PortScan"
cp $current/PortScan $previous/PortScan
trap "destructor && exit 1" SIGINT SIGTERM

cat $current/IPs | sudo naabu -silent -rate 1000 -retries 5 -timeout 1000 -top-ports 100 -verify -source-ip 10.10.10.10 -json -o $buffer & 
local naabu_pid=$!
echo "$INF Started Naabu..."
wait

destructor

}
