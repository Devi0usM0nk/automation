#!/bin/bash


CIDRScan (){
local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
source ./functions.sh
local FTL="[CIDRScan:$tmp_random_id][FTL]"
local INF="[CIDRScan:$tmp_random_id][INF]"
destructor (){
	if ps -p $naabu_pid &> /dev/null; then sudo kill -9 $naabu_pid &>/dev/null; fi
	if ps -p $dnsx_pid &> /dev/null; then kill -9 $dnsx_pid &>/dev/null; fi
	[[ -f $tmp/PortScan$tmp_random_id ]] && cat $tmp/PortScan$tmp_random_id $current/PortScan | sort -u -o $current/PortScan && rm $tmp/PortScan$tmp_random_id && echo "$INF Stored Port Scan output to $current/PortScan"
	[[ -f $tmp/IPs$tmp_random_id ]] && cat $tmp/IPs$tmp_random_id $current/IPs | sort -u -o $current/IPs && rm $tmp/IPs$tmp_random_id && echo "$INF Stored IPs output to $current/IPs"
	[[ -f $tmp/PTR$tmp_random_id ]] && cat $tmp/PTR$tmp_random_id $current/PTR | sort -u -o $current/PTR && rm $tmp/PTR$tmp_random_id && echo "$INF Stored PTR output to $current/PTR"
	[[ -f $tmp/subdomains$tmp_random_id ]] && cat $tmp/subdomains$tmp_random_id $current/subdomains | sort -u -o $current/subdomains && rm $tmp/subdomains$tmp_random_id && echo "$INF Stored subdomains output to $current/subdomains"
	[[ -f $buffer ]] && rm $buffer
	[[ -f $buffer.json ]] && sudo rm $buffer.json
	echo "$INF Cleared temporary Files"
}


echo "$INF CIDRScan Started"
[[ ! -f $current/CIDR ]] && echo "$FTL $current/CIDR does not exists" && exit 1
[[ ! -f $current/PortScan ]] && touch $current/PortScan && echo "$INF Created $current/PortScan"
[[ ! -f $current/IPs ]] && touch $current/IPs && echo "$INF Created $current/IPs"
[[ ! -f $current/PTR ]] && touch $current/PTR && echo "$INF Created $current/PTR"
[[ ! -f $current/subdomains ]] && touch $current/subdomains && echo "$INF Created $current/subdomains"
cp $current/PortScan $previous/PortScan
cp $current/IPs $previous/IPs
cp $current/PTR $previous/PTR
cp $current/subdomains $previous/subdomains
trap "destructor && exit 1" SIGINT SIGTERM

mapcidr -silent -l $current/CIDR | sudo naabu -silent -rate 250 -retries 5 -timeout 1000 -json -top-ports 100 -verify -source-ip 10.10.10.10 -o $buffer &>/dev/null &
local naabu_pid=$!
echo "$INF Started naabu..."
wait
sudo chown $USER $buffer.json && mv $buffer.json $buffer
not_in $current/PortScan >> $tmp/PortScan$tmp_random_id 

cat $tmp/PortScan$tmp_random_id | awk -F \" '{print $8}' > $buffer 
not_in $current/IPs >>  $tmp/IPs$tmp_random_id 

cat $tmp/IPs$tmp_random_id | dnsx -silent -ptr -resp-only | dnsx -silent -a -resp  > $buffer &
local dnsx_pid=$!
echo "$INF Started dnsx..."
wait
not_in $current/PTR >> $tmp/PTR$tmp_random_id  

cat $current/rootDomains | xargs -n1 -I{} sh -c "cat $tmp/PTR$tmp_random_id | grep '.{}'" > $buffer
not_in $current/subdomains >> $tmp/subdomains$tmp_random_id

destructor
}
