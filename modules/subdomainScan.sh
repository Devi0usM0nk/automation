#!/bin/bash 

subdomainScan(){
	local tmp_random_id=$(($(shuf -i 1-$RANDOM -n 1)*$RANDOM))
	local INF="[SubdomainScan:$tmp_random_id][INF]"
	local FTL="[SubdomainScan:$tmp_random_id][FTL]"
	local httpx_pid=""
	local naabu_pid=""
	local burp_pid=$(ps aux | grep burp | grep -v 'grep' | awk '{print $2}') 
	buffer=$(echo "$tmp/buffer$tmp_random_id")

	destructor(){
		if ps -p $naabu_pid &> /dev/null; then  kill -9 $naabu_pid 1> /dev/null ;fi
		if ps -p $httpx_pid &> /dev/null; then kill -9 $httpx_pid 1> /dev/null ;fi
		[[ -f "$tmp/HostScanbuffer$tmp_random_id.json" ]] &&  mv "$tmp/HostScanbuffer$tmp_random_id.json" "$buffer" && not_in "$current/HostScan" >> "$tmp/HostScan$tmp_random_id" 
		[[ -f "$tmp/httpxbuffer$tmp_random_id" ]] && mv "$tmp/httpxbuffer$tmp_random_id" "$buffer" && not_in "$current/httpxOut" >> "$tmp/httpxOut$tmp_random_id"
		[[ -f "$tmp/httpxOut$tmp_random_id" ]] && cat "$tmp/httpxOut$tmp_random_id" "$current/httpxOut" | sort -u -o "$current/httpxOut" && rm "$tmp/httpxOut$tmp_random_id" && echo "$INF Stored Httpx output to $current/httpxOut"
		[[ -f "$tmp/HostScan$tmp_random_id" ]] && cat "$tmp/HostScan$tmp_random_id" "$current/HostScan" | sort -u -o "$current/HostScan" && rm "$tmp/HostScan$tmp_random_id" && echo "$INF Stored Naabu output to $current/HostScan"
		rm "$buffer"
		echo "$INF Cleared temporary files"
	}
	
	[[ ! -f "$current/subdomains" ]] && echo "$FTL $current/subdomains does not exists \!\!\!" && exit 1
	[[ ! -f "$current/httpxOut" ]] && touch "$current/httpxOut" && echo "$INF Created $current/httpxOut"
	[[ ! -f "$current/HostScan" ]] && touch "$current/HostScan" && echo "$INF Created $current/HostScan"
	cp "$current/httpxOut" "$previous/httpxOut"
	cp "$current/HostScan" "$previous/HostScan"
	echo "$INF Started subdomain scan"

	trap "destructor && exit 1" SIGINT SIGTERM
	
	if ps -p $burp_pid &> /dev/null; then cat "$current/subdomains"  | awk '{print $1}' | httpx -threads 250 -http2 -no-color -pipeline -title -status-code -tls-probe -content-type -web-server -sr -srd "$current/output"  -method -websocket -cdn -cname -follow-redirects -retries 10 -http-proxy http://127.0.0.1:8080 -silent -o "$tmp/httpxbuffer$tmp_random_id" &
	else cat "$current/subdomains"  | awk '{print $1}' | httpx -threads 250 -http2 -no-color -pipeline -title -status-code -tls-probe -content-type -web-server -sr -srd "$current/output"  -method -websocket -cdn -cname -follow-redirects -retries 10 -silent -o "$tmp/httpxbuffer$tmp_random_id" & fi
	local httpx_pid=$!
	echo "$INF Started Httpx..."

	cat "$current/subdomains" | awk '{print $1}' | naabu -c 250 -silent -rate 1000 -retries 5 -timeout 1000 -top-ports 100 -verify -source-ip 10.10.10.10 -json -o "$tmp/HostScanbuffer$tmp_random_id" &
	local naabu_pid=$!
	echo "$INF Started Nabbu..."
	wait
	destructor	
}
