FROM alpine
RUN apk add bash
RUN mkdir -p /root/.local/bin/modules
RUN mkdir -p /root/.config/
RUN touch -p /root/.config/config
COPY Hunter.sh /usr/local/bin/
COPY modules /root/.local/bin/modules
RUN export PATH=$PATH:/root/.local/bin
