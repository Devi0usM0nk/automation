#!/bin/bash
config=$HOME/.config/Hunter/config
source $config
source $HOME/.local/bin/modules/functions.sh
current="inp_op/current"
previous="inp_op/previous"
buffer="tmp/buffer$tmp_random_id"

usage(){
	echo "Usage: Hunter  -h | --help"
	echo "                             Show the program help"
	echo "               -v | --version"
	echo "                             Show the program Version"
	echo "               init Name"
	echo "                             Create a project named 'Name'"

}

init(){
	mkdir "$dir/$1"  
	mkdir "$dir/$1/inp_op"
	mkdir "$dir/$1/inp_op/current"
	mkdir "$dir/$1/inp_op/previous"
	mkdir "$dir/$1/tmp"
	resolvers 500 1000 > "$dir/$1/Resolvers"
	echo "$1=\"$dir/$1\"" >> $config
	sort -u $config -o $config
	echo "Created Project $1"
}

run (){
    echo "[INF]Starting Hunter <- $1"
    current=$(echo "$(eval echo $(echo "\$$1"))/inp_op/current")
    previous=$(echo "$(eval echo $(echo "\$$1"))/inp_op/previous")
    tmp=$(echo "$(eval echo $(echo "\$$1"))/tmp")
    buffer=$(echo "$tmp/buffer$tmp_random_id")
    echo "Temp: $tmp"
    scopeParser
    rootScan
    allScan
    subdomainScan  
    ssWeb &
    IPScan  
    RDNSLookup 
    wait
    }

if [[ $# -eq 0 ]];then usage && exit 1
else 

tempArray=( "$@" )
tempArray[$(($#+1))]="--"
set -- ${tempArray[@]}

while :
do
    case "$1" in
	-h | --help ) usage; shift;;
	-v | --version ) echo "Version is 0.1 Beta"; shift;;
	init ) init $2;shift 2;;
	run ) run $2; shift 2;;
	-- ) shift; break;;
	* ) echo "Unexpected option: $1";break;;
    esac
done
fi
